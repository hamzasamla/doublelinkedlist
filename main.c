/**********DOUBLE LINKED LIST*********/

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int value;
    struct node* next;
    struct node* prev;

};
struct node* start=NULL;
struct node* ptr;

void insertvalue()
{
    ptr=(struct node*)malloc(sizeof(struct node));
    printf("Enter value: ");
    scanf("%d",&ptr->value);
    ptr->next=NULL;
    if(start==NULL)
    {
        ptr->prev=NULL;
        start=ptr;
    }
    else
    {
        struct node* curr=start;
        while(curr->next!=NULL)
        {
            curr=curr->next;
        }
        curr->next=ptr;
        ptr->prev=curr;
    }
}
void insertFirst()
{
    ptr=(struct node*)malloc(sizeof(struct node));
    printf("Enter value: ");
    scanf("%d",&ptr->value);
    ptr->next=start;
    ptr->prev=NULL;
    if(start!=NULL)
    {
        start->prev=ptr;
        start=ptr;
    }
}
void printlist()
{
    printf("Printing.....\n");
    struct node* curr=start;
    while(curr!=NULL)
    {
        printf("%d\n",curr->value);
        curr=curr->next;
    }
}
void printReverse()
{
    printf("Printing in Reverse \n");
    struct node* curr=start;

    while(curr->next!=NULL)
    {
        curr=curr->next;
    }

    while(curr!=NULL)
    {
        printf("%d\n",curr->value);
        curr=curr->prev;
    }



}
void insertAfter(struct node* prv)
{
    if(prv==NULL)
    {
        printf("It cant be null");
        return;
    }
    ptr=(struct node*)malloc(sizeof(struct node));
    printf("Enter value: ");
    scanf("%d",&ptr->value);
    ptr->next=prv->next;
    prv->next=ptr;
    ptr->prev=prv;
    if(ptr->next!=NULL)
    {
        (ptr->next)->prev=ptr;
    }
}

void swappin(struct node* start, int *p1, int *p2)

{
    if(p1==p2)
    {
        return;
    }
    struct node *prevp1=NULL, *currp1=start;
    while(currp1 && currp1->value!=p1)
    {
        prevp1=currp1;
        currp1=currp1->next;
    }
    struct node *prevp2=NULL, *currp2=start;
    while(currp2 && currp2->value!=p2)
    {
        prevp2=currp2;
        currp2=currp2->next;
    }
    if(currp1==NULL || currp2==NULL)
    {
        return;
    }

    if(prevp1!=NULL)
    {
        prevp1->next=currp2;

    }
    else
        {
        start=currp2;
    }
    if(prevp2!=NULL)
    {
        prevp2->next=currp1;
    }
    else{
        start=currp1;

    }
    struct node *temp=currp2->next;
    currp2->next=currp1->next;
    currp2->next=temp;
}
void insertion(struct node **start)
{

  struct node *curr=*start;
  curr=curr->next;
  *start=(*start)->next;
  while(*start!=NULL)
  {
    int hold = (*start)->value;
    curr=*start;
    while(hold < (curr->prev)->value)
    {
      curr->value=(curr->prev)->value;
      curr=curr->prev;
    }
    curr->value=hold;
    *start=(*start)->next;
  }
}
/*void insertSort()
{
    struct node* curr;
    while(curr->next!=NULL)
    {

    //    int hold=start;
        curr=start;
        while(curr<curr->next)
        {
            curr=curr->next;

        }
        curr->next=curr;

    }
}*/
int main()
{
    insertvalue();
    insertvalue();
    insertvalue();
    insertvalue();
    insertvalue();

    //insertFirst();
    printlist();
    //insertAfter(start->next);
    //printlist();

    //swappin(start,5,10);

    insertion(&start);

    printlist();


}
